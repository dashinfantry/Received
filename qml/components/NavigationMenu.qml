import QtQuick 2.6
import "../pages/"
import "./js/Utils.js" as Utils

NavigationMenuForm {
    property var settingsAction: function() { pageStack.push(Qt.resolvedUrl("../pages/SettingsPage.qml")); }
    property var showPlayerAction: function() { player.open = true; }
    property var browseAction: function() { pageStack.push(Qt.resolvedUrl("../pages/BrowsePage.qml"), {listType: Utils.Top100}); }
    property var searchAction: function() { pageStack.push(Qt.resolvedUrl("../pages/StationsPage.qml"), {listType: Utils.Search}); }


    actionSettings.onClicked: settingsAction()
    actionShowPlayer.onClicked: showPlayerAction()
    actionBrowse.onClicked: browseAction()
    actionSearch.onClicked: searchAction()
}
