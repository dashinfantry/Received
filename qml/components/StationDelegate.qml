import QtQuick 2.6
import Sailfish.Silica 1.0

ListItem {
    property string stationThumbnail: ""
    property string stationTitle: ""
    property string stationInfo: ""
    property string stationCurrent: ""
    property bool isPlaying: false

    width: parent.width
    contentHeight: Theme.itemSizeLarge

    Rectangle {
        color: isPlaying ? Theme.primaryColor : "transparent"
        opacity: 0.1
        anchors.fill: parent
    }

    Row {
        spacing: Theme.paddingMedium
        anchors {
            fill: parent
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }

        Image {
            id: image
            source: stationThumbnail

            smooth: true
            fillMode: Image.PreserveAspectFit
            cache: true

            sourceSize.width: Theme.iconSizeLarge
            sourceSize.height: Theme.iconSizeLarge
            height: parent.height
            width: height
        }

        Column {
            height: parent.height
            width: parent.width - x - parent.leftPadding

            Label {
                width: parent.width
                truncationMode: TruncationMode.Fade
                text: stationTitle
            }

            Label {
                width: parent.width
                font.pixelSize: Theme.fontSizeExtraSmall
                truncationMode: TruncationMode.Fade
                color: Theme.secondaryColor
                text: stationInfo
                visible: stationInfo
            }

            Label {
                width: parent.width
                font.pixelSize: Theme.fontSizeSmall
                truncationMode: TruncationMode.Fade
                color: Theme.secondaryHighlightColor
                text: stationCurrent
                visible: stationCurrent
            }
        }
    }
}
