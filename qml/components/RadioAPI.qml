import QtQuick 2.6
import io.thp.pyotherside 1.4
import "./js/Rad.js" as Rad
import "./js/Favorites.js" as FavoritesUtils

Python {
    id: radioAPI

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('./python'));

        setHandler('log', function(msg) {
            console.log("Python log:", msg)
        });

        setHandler('debug', function(msg) {
            console.debug("Python debug:", msg)
        });

        importModule('api', function () {
            setApiBaseUrl()
        });
    }

    function setApiBaseUrl() {
        call('api.radio.setApiBaseUrl', [settings.value("apiUrl")], function(response) {
            console.log("apiBaseUrl updated: ", response)
        });
    }

    function playStationById(id) {
        app.loading = true;
        call('api.radio.getStationById', [''+id], function(response) {
            var station = Rad.getStationFromRadioJson(response);
            console.debug("Playing station:", JSON.stringify(station))

            player.play(station);
            app.loading = false;
        });
    }

    function addRadIoAsFavorite(id) {
        app.loading = true;
        call('api.radio.getStationById', [''+id], function(response) {
            var station = Rad.getStationFromRadioJson(response);
            FavoritesUtils.addFavorite(station)

            app.loading = false
        });
    }

    function updateFavorite(oldFavorite) {
        app.loading = true;
        call('api.radio.getStationById', [''+oldFavorite.radIoId], function(response) {
            var station = Rad.getStationFromRadioJson(response);
            FavoritesUtils.removeFavorite(oldFavorite);
            FavoritesUtils.addFavorite(station);
            console.log("Updated", JSON.stringify(oldFavorite));

            app.loading = false;
        });
    }

    function getGenres(mList) {
        getOptionsByCategoryAndUpdateList("api.radio.getGenres", "genre", mList)
    }

    function getTopics(mList) {
        getOptionsByCategoryAndUpdateList("api.radio.getTopics", "topic", mList)
    }

    function getCountries(mList) {
        getOptionsByCategoryAndUpdateList("api.radio.getCountries", "country", mList)
    }

    function getCities(mList) {
        getOptionsByCategoryAndUpdateList("api.radio.getCities", "city", mList)
    }

    function getLanguages(mList) {
        getOptionsByCategoryAndUpdateList("api.radio.getLanguages", "language", mList)
    }

    function getOptionsByCategoryAndUpdateList(method, category, mList) {
        app.loading = true;
        call(method, [], function(response) {
            console.log("Number of results:", response.length)
            console.debug(JSON.stringify(response))
            mList.clear()

            response.forEach(function(response) {
                mList.append({ title: response.localized, name: response.systemEnglish, category: category})
            });

            app.loading = false
        });
    }

    function getTop100(mList) {
        getStationsAndUpdateList("api.radio.getTopStations", [], mList);
    }

    function getRecomended(mList) {
        getStationsAndUpdateList("api.radio.getRecomendedStations", [], mList);
    }

    function getLocal(mList) {
        getStationsAndUpdateList("api.radio.getLocalStations", [], mList)
    }

    function getStationByCategory(category, value, mList) {
        console.debug("Get stations by category", category, value)
        switch(category.toLocaleLowerCase()) {
        case "genre":
            getStationsAndUpdateList("api.radio.getStationsByGenre", [value], mList)
            break;
        case "topic":
            getStationsAndUpdateList("api.radio.getStationsByTopic", [value], mList)
            break;
        case "country":
            getStationsAndUpdateList("api.radio.getStationsByCountry", [value], mList)
            break;
        case "city":
            getStationsAndUpdateList("api.radio.getStationsByCity", [value], mList)
            break;
        case "language":
            getStationsAndUpdateList("api.radio.getStationsByLanguage", [value], mList)
            break;
        default:
            console.error("Getting stations by unknown category", category, value)
            break;
        }
    }

    function search(query, mList) {
        getStationsAndUpdateList("api.radio.getStationsByQuery", [query], mList)
    }

    function getStationsAndUpdateList(method, params, mList) {
        app.loading = true;
        call(method, params, function(response) {
            console.log("Number of results:", response.length)
            console.debug(JSON.stringify(response))
            mList.clear()

            response.forEach(function(station) {
//                if(station.playable === "PLAYABLE") {
                    mList.append(station)
//                }
            });

            app.loading = false
        });
    }

    onError: {
        console.log('python error: ' + traceback);
    }

    onReceived: {
        console.log('got message from python: ' + data);
    }
}
