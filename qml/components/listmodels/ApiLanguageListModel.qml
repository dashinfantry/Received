import QtQuick 2.0

ListModel {
     id: model
     ListElement { title: qsTr("Austrian"); apiUrl: "http://api.radio.at" }
     ListElement { title: qsTr("Danish"); apiUrl: "http://api.radio.dk" }
     ListElement { title: qsTr("English"); apiUrl: "http://api.radio.net" }
     ListElement { title: qsTr("French"); apiUrl: "http://api.radio.fr" }
     ListElement { title: qsTr("German"); apiUrl: "http://api.radio.de" }
     ListElement { title: qsTr("Italian"); apiUrl: "http://api.radio.it" }
     ListElement { title: qsTr("Polish"); apiUrl: "http://api.radio.pl" }
     ListElement { title: qsTr("Portuguese"); apiUrl: "http://api.radio.pt" }
     ListElement { title: qsTr("Spanish"); apiUrl: "http://api.radio.es" }
     ListElement { title: qsTr("Swedish"); apiUrl: "http://api.radio.se" }
 }
