import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components/"
import "../components/contextmenus" as ContextMenu


Page {
    id: favoritePage

    signal playFavorite(int index)
    signal removeFavorite(int index)
    signal editFavorite(int index)

    allowedOrientations: Orientation.All
    property bool firstPage: true
    property alias favoriteModel: favoriteModel
    property alias addCustomFavorite: actionAddCustomFavorite

    SilicaListView {
        id: favoritesListView
        VerticalScrollDecorator { flickable: favoritesListView }

        anchors.fill: parent
        spacing: Theme.paddingMedium

        NavigationMenu {
            id: navigationMenu

            MenuItem {
                id: actionAddCustomFavorite
                text: qsTr("Add Custom")

            }
        }

        header: Column {
                    PageHeader {
                        title: qsTr("Favorites")
                        width: favoritePage.width
                    }
                }

        model: ListModel {
            id: favoriteModel
        }

        delegate: StationDelegate {
            id: favoritItem

            stationThumbnail: stationLogo
            stationTitle: name
            stationInfo: qsTr("From") + " " + country + ": " + genre
            isPlaying: app.stationData !== undefined && app.stationData.url === url

            menu: ContextMenu.FavoritesListContextMenu {
                id: contextMenu
            }

            Connections {
                target: favoritItem
                onClicked: playFavorite(index)
            }

            Connections {
                target: contextMenu
                onRemoveFavorite: removeFavorite(index)
            }

            Connections {
                target: contextMenu
                onEditFavorite: editFavorite(index)
            }

        }
    }
}



