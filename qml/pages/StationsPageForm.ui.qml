import QtQuick 2.6
import Sailfish.Silica 1.0
import "../components"
import "../components/contextmenus" as ContextMenu


Page {
    id: page

    signal playStation(int id)
    signal removeFavorite(var station)
    signal addFavorite(int id)
    signal search(string text)

    property alias navigationMenu: navigationMenu
    property alias stationModel: stationModel

    property var listType
    property bool showSearch: false
    property string category: ""
    property string value: ""

    SilicaListView {
        id: stationListView
        VerticalScrollDecorator { flickable: stationListView }

        anchors.fill: parent
        spacing: Theme.paddingMedium

        NavigationMenu {
            id: navigationMenu
            hideSearchAction: showSearch
        }

        header: Column {
                    PageHeader {
                        title: qsTr(listType.header)
                        width: page.width
                    }

                    CustomSearchField {
                        id: searchField
                        width: page.width
                        visible: showSearch

                        Connections {
                            target: searchField
                            onSearch: search(text)
                        }
                    }
                }

        model: ListModel {
            id: stationModel
        }

        delegate: StationDelegate {
            id: stationItem

            stationThumbnail: thumbnail
            stationTitle: name
            stationInfo: qsTr("From") + " " + country + ": " + genres
            stationCurrent: nowPlaying
            isPlaying: app.stationData !== undefined && app.stationData.radIoId === id

            menu: ContextMenu.StationsListContextMenu {
                id: contextMenu
            }

            Connections {
                target: stationItem
                onClicked: playStation(id)
            }

            Connections {
                target: contextMenu
                onRemoveFavorite: removeFavorite(station)
            }

            Connections {
                target: contextMenu
                onAddFavorite: addFavorite(id)
            }
        }
    }
}
