#ifndef MPRISPLAYER_H
#define MPRISPLAYER_H

#include <QObject>
#include <QDBusAbstractAdaptor>
#include <QMediaPlayer>
#include "audioplayer.h"

class MprisPlayer : public QDBusAbstractAdaptor
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.mpris.MediaPlayer2.Player")

    Q_PROPERTY(bool CanControl READ CanControl)
    Q_PROPERTY(bool CanPlay READ CanPlay)
    Q_PROPERTY(bool CanPause READ CanPause)
    Q_PROPERTY(bool CanGoNext READ CanGoNext)
    Q_PROPERTY(bool CanGoPrevious READ CanGoPrevious)
    Q_PROPERTY(bool CanSeek READ CanSeek)
    Q_PROPERTY(bool Shuffle READ Shuffle WRITE setShuffle)
    Q_PROPERTY(qint64 Position READ Position)
    Q_PROPERTY(double MinimumRate READ MinimumRate)
    Q_PROPERTY(double MaximumRate READ MaximumRate)
    Q_PROPERTY(double Rate READ Rate WRITE setRate)
    Q_PROPERTY(double Volume READ Volume WRITE setVolume)

    Q_PROPERTY(QString PlaybackStatus READ PlaybackStatus)
    Q_PROPERTY(QString LoopStatus READ LoopStatus WRITE setLoopStatus)
    Q_PROPERTY(QVariantMap Metadata READ Metadata)

public:
    explicit MprisPlayer(AudioPlayer *parent);

    bool CanControl() { return true; }
    bool CanPlay() { return m_player->isPlayable(); }
    bool CanPause() { return true; }
    bool CanGoNext() { return false; }
    bool CanGoPrevious() { return false; }
    bool CanSeek() { return false; }
    bool Shuffle() const { return false; }

    void setShuffle(const bool);
    qint64 Position();
    double MinimumRate();
    double MaximumRate();
    double Rate();
    double Volume();
    void setRate(const double);
    void setVolume(const double);

    QString PlaybackStatus();
    QString LoopStatus();
    void setLoopStatus(QString);
    QVariantMap Metadata();

private:
    AudioPlayer *m_player;
    QMediaPlayer *m_qmediaplayer;

public slots:
    void Play();
    void PlayPause();
    void Pause();
    void Stop();
    void Next();
    void Previous();

    void OpenUri(QString);
    void Seek(qint64);
    void SetPosition(QString, qint64);

private slots:
    void playbackStatusChanged();
    void metaDataChanged();
    void notifyPropertyChanged(const QVariantMap &);
};

#endif // MPRISPLAYER_H
